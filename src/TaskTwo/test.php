<?php

class  UserEmailChangerService
{
    private \PDO $db;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param int $userId
     * @param string $email
     *
     * @return void
     *
     * @throws Exception
     */
    public function changeEmail(int $userId, string $email): void
    {
        try {
            $this->db->beginTransaction();

            $statement = $this->db->prepare('SELECT email FROM users WHERE id = :id FOR UPDATE');
            $statement->bindParam(':id', $userId, \PDO::PARAM_INT);
            $statement->execute();
            $oldEmail = $statement->fetch(\PDO::FETCH_COLUMN);
            if (!$oldEmail) {
                throw new \Exception('User not found');
            }
            if ($oldEmail === $email) {
                throw new \Exception('Email are same');
            }

            $statement = $this->db->prepare('UPDATE users SET email = :email WHERE id = :id');

            $statement->bindParam(':id', $userId, \PDO::PARAM_INT);
            $statement->bindParam(':email', $email, \PDO::PARAM_STR);
            $statement->execute();

            /** @var UserEmailSenderInterface $emailSender */
            $emailSender = new UserEmailSender();
            $emailSender->sendEmailChangedNotification($oldEmail, $email);

            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();
            throw $e;
        }
    }
}

interface  UserEmailSenderInterface
{
    /**
     * @param string $oldEmail
     * @param string $newEmail
     *
     * @return void
     *
     * @throws EmailSendException
     */
    public function sendEmailChangedNotification(string $oldEmail, string $newEmail): void;
}

