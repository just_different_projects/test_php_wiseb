<?php

namespace TaskOne\User;

use Exception;
use TaskOne\Core\AbstractTable;

/**
 * @method null|UserModel getById(int $id, bool $forUpdate = false)
 * @method UserModel[] getList(?array $filter = null, int $limit = 50, int $offset = 0)
 *
 */
class UserTable extends AbstractTable
{
    static function getTableName(): string
    {
        return 'users';
    }

    /**
     * @throws Exception
     */
    protected function rowToModel(array $row): UserModel
    {
        return new UserModel(
            $row['id'],
            $row['name'],
            $row['email'],
            new \DateTime($row['created']),
            $row['deleted'] ? new \DateTime($row['deleted']) : null,
            $row['notes'],
        );
    }

    public function save(UserModel $user): int
    {
        $data = [
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'deleted' => $user->getDeleted()?->format('Y-m-d H:i:s'),
            'notes' => $user->getNotes(),
        ];
        if (!$user->getId()) {
            // возможность установки этой даты может зависить от требований, может выставляться на уровне базы данных, тут просто для примера
            $data['created'] = $user->getCreated()->format('Y-m-d H:i:s');
            return parent::create($data);
        } else {
            return parent::update($user->getId(), $data);
        }
    }
}