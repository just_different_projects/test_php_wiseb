<?php

namespace TaskOne\User\Factory;

use TaskOne\Core\StringValidatorInterface;
use TaskOne\User\DefaultEmailValidator;

class EmailValidatorFactory
{
    public function build(): StringValidatorInterface {
        return new DefaultEmailValidator();
    }
}