<?php

namespace TaskOne\User\Factory;

use TaskOne\Core\StringValidatorInterface;
use TaskOne\User\DefaultUserNameValidator;

class UserNameValidatorFactory
{
    public function build(): StringValidatorInterface {
        return new DefaultUserNameValidator();
    }
}