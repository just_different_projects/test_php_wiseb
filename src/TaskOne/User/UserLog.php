<?php

namespace TaskOne\User;

class UserLog
{
    private static ?UserLog $instance = null;

    public static function getInstance(): UserLog
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function __construct()
    {

    }

    /**
     * Just for example
     * @param UserModel $user
     * @return void
     */
    public function log(UserModel $user): void {
        // any logging
    }
}