<?php

namespace TaskOne\User;

use InvalidArgumentException;

trait ValidateUserNameTrait
{
    const array FORBIDDEN_WORDS = [
        'badword'
    ];

    /**
     * @throws InvalidArgumentException
     */
    public function validateUserName(string $name): void
    {
        if (strlen($name) < 8) {
            throw new InvalidArgumentException('Too short name');
        }
        if (!preg_match("/^[a-z0-9]+$/", $name)) {
            throw new InvalidArgumentException('Invalid name');
        }
        $lowerName = strtolower($name);
        foreach (self::FORBIDDEN_WORDS as $word) {
            if (str_contains($lowerName, $word)) {
                throw new InvalidArgumentException('Name has forbidden word');
            }
        }
    }
}