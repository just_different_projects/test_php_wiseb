<?php

namespace TaskOne\User;

use InvalidArgumentException;
use TaskOne\Core\StringValidatorInterface;

class DefaultEmailValidator implements StringValidatorInterface
{
    const array INVALID_EMAIL_LIST = [
        // Invalid email list, just for example
    ];

    /**
     * @throws InvalidArgumentException
     */
    public function validate(string $value): void
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('Invalid email');
        }
        if (in_array($value, self::INVALID_EMAIL_LIST)) {
            throw new InvalidArgumentException('Invalid email');
        }
    }
}