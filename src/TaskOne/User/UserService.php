<?php

namespace TaskOne\User;

use TaskOne\Core\DBFactory;
use TaskOne\Core\DBInterface;
use TaskOne\Core\StringValidatorInterface;
use TaskOne\User\Factory\EmailValidatorFactory;
use TaskOne\User\Factory\UserNameValidatorFactory;

class UserService
{
    private UserTable $userTable;
    private DBInterface $db;
    private StringValidatorInterface $emailValidator;
    private StringValidatorInterface $userNameValidator;

    public function __construct()
    {
        $emailValidatorFactory = new EmailValidatorFactory();
        $this->emailValidator = $emailValidatorFactory->build();

        $userNameValidatorFactory = new UserNameValidatorFactory();
        $this->userNameValidator = $userNameValidatorFactory->build();

        $this->userTable = new UserTable();

        $dbFactory = new DBFactory();
        $this->db = $dbFactory->build();
    }

    public function getUsers(): array
    {
        return $this->userTable->getList(['deleted' => 'NULL']);
    }

    public function getUser(int $id): ?UserModel
    {
        return $this->userTable->getById($id);
    }

    public function createUser($name, $email, $notes = ''): int
    {
        $this->emailValidator->validate($email);
        $this->userNameValidator->validate($name);

        if ($this->userTable->getOneBy(['name' => $name])) {
            throw new \InvalidArgumentException('User with this name already exists');
        }

        if ($this->userTable->getOneBy(['email' => $email])) {
            throw new \InvalidArgumentException('User with this email already exists');
        }

        $user = new UserModel();
        $user->setName($name);
        $user->setEmail($email);
        $user->setNotes($notes);
        return $this->userTable->save($user);
    }

    public function updateUser($id, $data): int|false
    {
        $this->db->getPdo()->beginTransaction();
        try {
            $user = $this->userTable->getById($id, true);
            if ($user) {
                if (isset($data['name'])) {
                    $this->userNameValidator->validate($data['name']);
                    $user->setName($data['name']);
                }
                if (isset($data['email'])) {
                    $this->emailValidator->validate($data['email']);
                    $user->setEmail($data['email']);
                }

                // Возможно логичнее возврат пользователя а не ид, но будем считать что это уже тонкости реализации
                $userId = $this->userTable->save($user);
                UserLog::getInstance()->log($user);
                return $userId;
            }
            $this->db->getPdo()->commit();
        } catch (\Exception $e) {
            $this->db->getPdo()->rollBack();
        }
        return false;
    }

    public function deleteUser(int $id): void
    {
        $user = $this->userTable->getById($id);
        $user->setDeleted(new \DateTime());
        $this->userTable->save($user);
        UserLog::getInstance()->log($user);
    }
}