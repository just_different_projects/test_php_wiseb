<?php

namespace TaskOne\User;

use InvalidArgumentException;

trait ValidateEmailTrait
{
    const array INVALID_EMAIL_LIST = [
        // Invalid email list, just for example
    ];

    /**
     * @throws InvalidArgumentException
     */
    public function validateEmail(string $email): void
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('Invalid email');
        }
        if (in_array($email, self::INVALID_EMAIL_LIST)) {
            throw new InvalidArgumentException('Invalid email');
        }
    }
}