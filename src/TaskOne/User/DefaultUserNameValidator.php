<?php

namespace TaskOne\User;

use InvalidArgumentException;
use TaskOne\Core\StringValidatorInterface;

class DefaultUserNameValidator implements StringValidatorInterface
{
    const array FORBIDDEN_WORDS = [
        'badword'
    ];

    /**
     * @throws InvalidArgumentException
     */
    public function validate(string $value): void
    {
        if (strlen($value) < 8) {
            throw new InvalidArgumentException('Too short name');
        }
        if (!preg_match("/^[a-z0-9]+$/", $value)) {
            throw new InvalidArgumentException('Invalid name');
        }
        $lowerName = strtolower($value);
        foreach (self::FORBIDDEN_WORDS as $word) {
            if (str_contains($lowerName, $word)) {
                throw new InvalidArgumentException('Name has forbidden word');
            }
        }
    }
}