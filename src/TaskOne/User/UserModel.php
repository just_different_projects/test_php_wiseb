<?php

namespace TaskOne\User;

use TaskOne\Core\AbstractModel;

class UserModel extends AbstractModel
{
    public function __construct(
        private readonly ?int $id = null,
        private string        $name = '',
        private string        $email = '',
        private \DateTime     $created = new \DateTime(),
        private ?\DateTime    $deleted = null,
        private string        $notes = '',
    )
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): static
    {
        $this->created = $created;
        return $this;
    }

    public function getDeleted(): ?\DateTime
    {
        return $this->deleted;
    }

    public function setDeleted(?\DateTime $deleted): static
    {
        $this->deleted = $deleted;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;
        return $this;
    }

    public function getNotes(): string
    {
        return $this->notes;
    }

    public function setNotes(string $notes): static
    {
        $this->notes = $notes;
        return $this;
    }
}