<?php declare(strict_types=1);

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use TaskOne\User\DefaultUserNameValidator;

final class DefaultUserNameValidatorTest extends TestCase
{
    public static function invalidNamesProvider(): array
    {
        return [
            ['userTest'],
            ['user'],
            ['useruser$'],
            ['user@user'],
            ['badworduser'],
        ];
    }

    #[DataProvider('invalidNamesProvider')]
    public function testInvalidUserName(string $userName): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $validator = new DefaultUserNameValidator();
        $validator->validate($userName);
    }

    public static function validNamesProvider(): array
    {
        return [
            ['usertest'],
            ['usertest1'],
        ];
    }

    #[DataProvider('validNamesProvider')]
    public function testValidUserName(string $userName): void
    {
        $this->expectNotToPerformAssertions();
        $validator = new DefaultUserNameValidator();
        $validator->validate($userName);
    }
}