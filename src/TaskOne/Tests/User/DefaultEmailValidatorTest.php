<?php declare(strict_types=1);

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use TaskOne\User\DefaultEmailValidator;
use TaskOne\User\DefaultUserNameValidator;

final class DefaultEmailValidatorTest extends TestCase
{
    public static function invalidEmailsProvider(): array
    {
        return [
            ['email'],
            ['email@test'],
        ];
    }

    #[DataProvider('invalidEmailsProvider')]
    public function testInvalidEmails(string $email): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $validator = new DefaultEmailValidator();
        $validator->validate($email);
    }

    public static function validEmailsProvider(): array
    {
        return [
            ['test@test.com'],
        ];
    }

    #[DataProvider('validEmailsProvider')]
    public function testValidEmails(string $email): void
    {
        $this->expectNotToPerformAssertions();
        $validator = new DefaultEmailValidator();
        $validator->validate($email);
    }
}