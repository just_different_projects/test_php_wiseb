<?php

namespace TaskOne\Core;

use PDO;

abstract class AbstractTable
{
    use FilterTrait;

    protected DBInterface $db;

    public function __construct()
    {
        $dbFactory = new DBFactory();
        $this->db = $dbFactory->build();
    }

    abstract protected static function getTableName(): string;

    abstract protected function rowToModel(array $row): ?AbstractModel;

    public function getList(
        array $filter = null,
        int   $limit = 50,
        int  $offset = 0,
        bool $forUpdate = false
    ): array
    {
        $tableName = static::getTableName();
        $sql = "SELECT * FROM $tableName";

        if (is_array($filter)) {
            [$str, $filter] = $this->parseFilter($filter);
            $sql .= " WHERE " . $str;
        }

        $sql .= ' LIMIT ' . $limit . ' OFFSET ' . $offset;

        if ($forUpdate) {
            $sql .= ' FOR UPDATE';
        }

        $stmt = $this->db->getPdo()->prepare($sql);
        $stmt->execute($filter);
        $result = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $result[] = $this->rowToModel($row);
        }
        return [
            'data' => $result,
            'count' => $stmt->rowCount()
        ];
    }

    public function getById(int $id, bool $forUpdate = false): ?AbstractModel
    {
        return $this->getOneBy(['id' => $id], $forUpdate);
    }

    public function getOneBy(array $filter, bool $forUpdate = false): ?AbstractModel
    {
        $res = $this->getList($filter, 1, $forUpdate);

        return $res['data'][0] ?? null;
    }

    protected function create(array $data): int
    {
        $tableName = static::getTableName();
        $columns = implode(", ", array_keys($data));
        $values = implode(", :", array_keys($data));
        $sql = "INSERT INTO $tableName ($columns) VALUES (:$values)";
        $stmt = $this->db->getPdo()->prepare($sql);
        $stmt->execute($data);
        return (int)$this->db->getPdo()->lastInsertId();
    }

    protected function update(int $id, array $data): int
    {
        $tableName = static::getTableName();

        $columns = implode(", ", $this->getArrayValuesForStatement($data));

        $sql = "UPDATE $tableName SET $columns WHERE id = :id";
        $stmt = $this->db->getPdo()->prepare($sql);

        $data['id'] = $id;
        $stmt->execute($data);

        return $id;
    }

    protected function getArrayValuesForStatement($data): array
    {
        $values = [];
        foreach ($data as $key => $value) {
            $values[] = "$key = :$key";
        }
        return $values;
    }
}