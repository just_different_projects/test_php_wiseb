<?php

namespace TaskOne\Core;

use InvalidArgumentException;

interface StringValidatorInterface
{
    /**
     * @throws InvalidArgumentException
     */
    public function validate(string $value): void;
}