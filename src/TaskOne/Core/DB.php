<?php

namespace TaskOne\Core;

// TODO to environments variable
const HOST = "127.0.0.1";
const DB_NAME = "wisedb";
const CHARSET = "utf8";
const PORT = "3311";
const USERNAME = "admin";
const PASSWORD = "1234";

class DB implements DBInterface
{
    public \PDO $pdo;

    private static ?DB $instance = null;

    public static function getInstance(): DB
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function getPdo(): \PDO
    {
        return $this->pdo;
    }

    public function __construct()
    {
        $this->pdo = new \PDO(
            dsn: "mysql:host=" . HOST . ";dbname=" . DB_NAME . ";charset=" . CHARSET . ";port=" . PORT,
            username: USERNAME,
            password: PASSWORD,
        );
    }
}