<?php

namespace TaskOne\Core;

trait FilterTrait
{
    protected function parseFilter(array $filter): array
    {
        $conditions = [];
        $values = [];
        foreach ($filter as $column => $value) {
            /**
             * Тут должен быть парсер различных условий по какому-нибудь принятому в проекте формату, сейчас просто пример
             */
            if ($value === 'NULL') {
                $conditions[]= "$column IS NULL";
            } else {
                $values[$column] = $value;
                $conditions[]= "$column = :$column";
            }
        }
        return [implode(' AND ', $conditions), $values];
    }
}