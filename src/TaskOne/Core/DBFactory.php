<?php

namespace TaskOne\Core;

class DBFactory
{
    public function build(): DBInterface {
        return DB::getInstance();
    }
}