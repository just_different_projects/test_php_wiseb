<?php

namespace TaskOne\Core;

use PDO;

interface DBInterface
{
    public function getPdo(): PDO;
}